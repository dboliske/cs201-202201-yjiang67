# Lab 0

## Total

17.5/20

## Break Down

* Eclipse "Hello World" program         5/5
* Correct TryVariables.java & run       3.5/4
* Name and Birthdate program            5/5
* Square
  * Pseudocode                          2/2
  * Correct output matches pseudocode   2/2
* Documentation                         0/2

## Comments
line 10 of TryVariables should be short, not int
Also remember to document your code