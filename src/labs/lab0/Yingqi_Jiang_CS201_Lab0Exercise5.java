package labs.lab0;

//Indicate the size of the square.
//Indicate the character you want to use for your square.
//Use System.out.println to print out the square by printing out the character repeatedly
//in the same line until it reaches the size you want for your square.
//Repeat the same step in a new line until you reach the size of the square.
public class Yingqi_Jiang_CS201_Lab0Exercise5 {

	public static void main(String[] args) {
		//size of square: 7
		// character: o
		System.out.println("ooooooo");
		System.out.println("ooooooo");
		System.out.println("ooooooo");
		System.out.println("ooooooo");
		System.out.println("ooooooo");
		System.out.println("ooooooo");
		System.out.println("ooooooo");
	}

}
