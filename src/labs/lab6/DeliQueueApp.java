package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class DeliQueueApp {

	// printing out the menu options 
	public static ArrayList<String> menu(Scanner input, ArrayList<String> customerList){
		boolean exist = false;
		do {
			System.out.println("1. Add customer to queue");
			System.out.println("2. Help customer");
			System.out.println("3. Exist");
			System.out.println("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
			case "1":
				customerList = addCustomer(customerList, input);
				break;
			case "2":
				customerList = helpCustomer(customerList, input);
				break;
			case "3":
				exist = true;
				break;
			default:
				System.out.println("Invalid choice, returning to menu...");
			}
			
		}while(!exist);
		return customerList;
	}
	
	// adding customer to the end of the line and returning their position
	public static ArrayList<String> addCustomer(ArrayList<String> customerList, Scanner input){
		System.out.println("Enter name of new customer: ");
		String name = input.nextLine();
		customerList.add(name);
		int position = customerList.indexOf(name);
		System.out.println("Postion of " + name + " in queue: " + position );
		return customerList;
	}
	
	// removing customer in front of the line and returning their name if there is one
	public static ArrayList<String> helpCustomer(ArrayList<String> customerList, Scanner input){
		if (customerList.size() >= 1) {
		String name = customerList.get(0);
		System.out.println("Helping " + name + "...");
		customerList.remove(0);
		
		} else {
			System.out.println("There's no one in the queue");
		}
		return customerList;
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// declare arraylist 
		ArrayList<String> customerList = new ArrayList<String>();
		// prompt menu
		customerList = menu(input, customerList);
		input.close();
	}

}
