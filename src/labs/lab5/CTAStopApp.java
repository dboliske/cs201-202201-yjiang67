package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class CTAStopApp {
	///////// reading file
	public static CTAStation[] readfile(String filename) {
		CTAStation[] cta = new CTAStation[10];
		int count = 0;
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(System.in);
			
			while(input.hasNextLine()) {
				String line = input.nextLine();
				cta = resize(cta,cta.length*2);
			}
			input.close();
		} catch(FileNotFoundException fnf) {
			System.out.println("file not found");
		} catch (Exception e) {
			//generic exception
				System.out.println("error reading the file");
		}
		cta = resize(cta,count);
		return cta;
	}
	/////// resizing array
	public static CTAStation[] resize(CTAStation[] data, int size) {
		CTAStation[] a = new CTAStation[size];
		int limit = data.length > size ? size : data.length; //if data length > size, then size=data length
		for (int i=0; i<limit; i++) {
			a[i] = data[i];
		}
		return a;
	}
	////// menu
	public static CTAStation[] menu(Scanner input, CTAStation[] data) {
		boolean exist = false;
		do {
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Stations with/without Wheelchair access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exist");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1":
					displayStationNames(data);
					break;
				case "2":
					displayByWheelchair(data);
					break;
				case "3":
					displayNearest(data);
					break;
				case "4":
					exist = true;
					break;
				default :
					System.out.println("invalid");
			}
		} while(!exist);
		
		return data;
	}
	
	public static void displayStationNames(CTAStation[] data) {
		for (int i=0; i<data.length; i++) {
			System.out.println(data[i].getName());
		}
	}
	
	public static void displayByWheelchair(Scanner input, CTAStation[] data) {
		boolean valid = false;
		do {
			System.out.println("Wheelchair accessibility(y or n): ");
			String access = input.next();
			switch (access.toLowerCase()) {
			case "y":
				for (int i=0; i<data.length; i++) {
					if (data[i].hasWheelchair() == true) {
						System.out.println(data[i]);
					} else {
						System.out.println("no stations found");
					}
				}
				valid = true;
				break;
			case "n":
				for (int i=0; i<data.length; i++) {
					if (data[i].hasWheelchair() == false) {
						System.out.println(data[i]);
					} else {
						System.out.println("no stations found");
					}
				}
				valid = true;
				break;
			default: 
				System.out.println("invalid input, enter again");
			}
		}while(!valid);
		
		input.close();
	}
	
	public static void displayNearest(Scanner input, CTAStation[] data) {
		System.out.println("enter latitude: ");
		double lat = Double.parseDouble(input.nextLine());
		System.out.println("enter longtitude: ");
		double lng = Double.parseDouble(input.nextLine());
		Geolocation x = new Geolocation(lat,lng);
			double distanceFirst = calcDistance(data[0].getLat, data[0].getLng());
			int loop = 0;
				for(int j=1; j<data.length; j++) {
					double distance = calcDistance(data[j].getLat, data[j].getLng());
					if (distance < distanceFirst) {
						distanceFirst = distance;
						loop = j;
					}
				} 
				System.out.println(data[loop].getName());
		}
	
	
	/////////////////  note  ///////////////////////// 
	//protected String csvData() {
		//return name + "," + age + "," + weight;
	//}
	
	//public String toCSV() {
		//return "Animal," + csvData();
	//}
	// public static void savefile(String filename, CTAStation[] data) {
		//try {
			//FileWriter writer = new FileWriter(filename);
			
			//for (int i=0; i<data.length; i++) {
				//writer.write(data[i].toCSV() + "\n");
				//writer.flush();
			//}
			
			//writer.close();
		//} catch (Exception e) {
			//System.out.println("Error saving to file.");
		//}
	//}
	
	public static void main(String[] args) {
		// read file
		Scanner input = new Scanner(System.in);
		System.out.println("enter file name: ");
		String filename = input.nextLine();
		CTAStation[] data = readfile(filename);
		// menu
		data = menu(input, data);
		
		input.close();
		System.out.println("ended");
	}

}
