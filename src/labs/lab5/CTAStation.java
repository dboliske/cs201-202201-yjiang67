package labs.lab5;

public class CTAStation {

private String name;
private String location;
private boolean wheelchair;
private boolean open;

public CTAStation() {
	name = "abc";
	location = "none";
	wheelchair = false;
	open = false;
}
public CTAStation(String n, double lag, double lng, String loc, boolean wc, boolean op) {
	name = n;
	location = loc;
	wheelchair = wc;
	open = op;
}

public String getName() {
	return name;
}
public String getLocation() {
	return location;
}
public boolean hasWheelchair() {
	return wheelchair;
}
public boolean isOpen() {
	return open;
}

public void setName(String name) {
	this.name = name;
}
public void setLocation(String location) {
	this.location = location;
}
public void setWheelchair(boolean wheelchair) {
	this.wheelchair = wheelchair;
}
public void setOpen(boolean open) {
	this.open = open;
}

public String toString() {
	return "Station: " + name + "(" + location + ")" + "\n Wheelchair access: " + wheelchair;
}

public boolean equals(CTAStation x) {
	if (this.name.equalsIgnoreCase(x.getName())) {
		return false;
	} else if (this.location.equalsIgnoreCase(x.getLocation())) {
		return false;
	} else if (this.wheelchair != x.hasWheelchair()) {
		return false;
	} else if (this.open != x.isOpen()) {
		return false;
	}
	return true;
}
	
}
