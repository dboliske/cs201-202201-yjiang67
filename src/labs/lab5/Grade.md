# Lab 5

## Total

18/20

## Break Down

CTAStation

- Variables:                    2/2
- Constructors:                 0/1
- Accessors:                    2/2
- Mutators:                     2/2
- toString:                     1/1
- equals:                       2/2

CTAStopApp

- Reads in data:                1/2
- Loops to display menu:        2/2
- Displays station names:       1/1
- Displays stations by access:  2/2
- Displays nearest station:     2/2
- Exits                         1/1

## Comments
Remember to use super() in your constructor so that the child class has access to the parent class variables and methods

For reading your file, you have to do a specific Scanner type. Instead of writing System.in in the () on line 16, it should be just the File variable which is f here.
