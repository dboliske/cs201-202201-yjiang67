package labs.lab1;

import java.util.Scanner;

public class Yingqi_Jiang_CS201_Lab1_01242022_Exercise6 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter length in inches: "); // prompt user for input
		double inch = Double.parseDouble(input.nextLine()); // convert user input to double
		double cm = inch * 2.54; // convert inches into cm
		System.out.println("Your length in cm is " + cm + "."); // output result to console
		input.close();

	}

}
// Test Table
// 1. input: 10, expected: 25.4, actual: 25.4
// 2. input: 100, expected: 254, actual: 254
// 3. input: 0.5, expected: 1.27, actual: 1.27
// The program results came out as expected.