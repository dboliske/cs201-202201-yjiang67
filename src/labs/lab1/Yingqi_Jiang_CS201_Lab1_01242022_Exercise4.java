package labs.lab1;

import java.util.Scanner;

public class Yingqi_Jiang_CS201_Lab1_01242022_Exercise4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		// Exercise 4
		// Fahrenheit to Celsius
				System.out.println("Enter temperature in Fahrenheit: "); // prompt user for input
				double tempF = Double.parseDouble(input.nextLine()); // convert user input to double
				double tempC = (tempF-32)*(0.56); // convert from F to C
				System.out.println("Temperature in Celsius is " + tempC + " degrees."); // output result to console
				
		// Celsius to Fahrenheit
				System.out.println("Enter temperature in Celsius: "); // prompt user for input
				double TempC =Double.parseDouble(input.nextLine()); // convert user input to double
				double TempF = (TempC*(1.8)) + 32; // convert from C to F
				System.out.println("Temperature in Fahrenheit is " + TempF + " degrees."); // output result to console
		input.close();
	}

}
// Test Table 
// 1. input: 100 degrees F, expected: 38.08, actual: 38.08
//    input: 38 degrees C, expected: 100.4, actual: 100.4
// 2. input: 10 degrees F, expected: -12.32, actual: -12.32
//    input: 0 degree C, expected: 32, actual: 32
// 3. input: 0 degrees F, expected: -17.92, actual: -17.92
//    input: -5 degrees C, expected: 23, actual: 23
//  The program results came out as expected.

