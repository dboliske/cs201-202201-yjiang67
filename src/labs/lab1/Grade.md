# Lab 1

## Total

20/20

## Break Down

* Exercise 1    2/2
* Exercise 2    2/2
* Exercise 3    1.5/2
* Exercise 4
  * Program     2/2
  * Test Plan   1/1
* Exercise 5
  * Program     2/2
  * Test Plan   1/1
* Exercise 6
  * Program     2/2
  * Test Plan   1/1
* Documentation 5/5

## Comments
For exercise 3, you need to do the charAt in a different line so that the input has a variable in it when doing chatAt. Right now, the program tries to find the charAt0 when there's no input so it reads out an error