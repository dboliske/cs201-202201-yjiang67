package labs.lab1;

import java.util.Scanner;

public class Yingqi_Jiang_CS201_Lab1_01242022_Exercise1to3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); 
		// Exercise 1
		System.out.println("Enter a name: "); // prompt user for input
		String name = input.nextLine(); // scan user's input
		System.out.println(name); // output result to console
		
		// Exercise 2
		System.out.println("Enter your age: "); // prompt user for input
		double age = Double.parseDouble(input.nextLine()); // convert user input into double 
		System.out.println("Enter your father's age: "); // prompt user for input
		double FatherAge = Double.parseDouble(input.nextLine()); // convert user input to double
		double AgeDiff = FatherAge - age; //Your age subtracted from your father's age
		System.out.println("Your father is " + AgeDiff + " years older than you."); // output result to console
		// Birth year multiply by 2
		System.out.println("Enter your birth year: "); // prompt user for input
		double x = Double.parseDouble(input.nextLine()); // convert user input to double
		double y = x*2;
		System.out.println("Your birth year multiply by 2 is " + y + "."); // output result to console
		// convert inches to cm
		System.out.println("Enter your height in inches: "); // prompt user for input
		double heightIn = Double.parseDouble(input.nextLine()); // convert user input to double
		double heightCm = heightIn*2.54; 
		System.out.println("Your height in cm is " + heightCm + "."); // output result to console
		// convert inches to feet
		System.out.println("Enter your height in inches: "); // prompt user for input
		int height = input.nextInt(); // store user input as an int vairable
		double heightFt = height/12;  // convert input to ft and store as double
		int inch = height%12; // getting the remainder inches 
		System.out.println("Your height is " + heightFt + " feet and " + inch + " inches."); // output result to console
		
		// Exercise 3
		System.out.println("Enter your first name: "); // prompt user for input
		char firstLetter = input.nextLine().charAt(0); // read+store the first letter of the user's input
		System.out.println("Your first initial is " + firstLetter + "."); // output result to console
		
		input.close();
	}



}
