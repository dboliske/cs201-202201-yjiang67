package labs.lab1;

import java.util.Scanner;

public class Yingqi_Jiang_CS201_Lab1_01242022_Exercise5 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter length of box in inches: "); // prompt user for input
		double length = Double.parseDouble(input.nextLine()); // convert user input to double
		System.out.println("Enter width of box in inches: "); // prompt user for input
		double width = Double.parseDouble(input.nextLine()); // convert user input to double
		System.out.println("Enter depth of box in inches: "); // prompt user for input
		double depth = Double.parseDouble(input.nextLine()); // convert user input to double
		double lengthFt = length/12; // convert inches to ft
		double widthFt = width/12; // convert inches to ft
		double depthFt = depth/12; // convert inches to ft
		double boxSA = lengthFt * widthFt * depthFt; // compute the surface area of box
		System.out.print("The amount of wood needed to build the box is " + boxSA + " square feet.");
		// output result to console
		input.close();

	}

}
// Test Table
// 1. length: 5, width: 10, depth: 3. expected: 0.0868, actual: 0.08680555555555557
// 2. length: 24, width: 14, depth: 12. expected: 2.3333, actual: 2.3333333333333335
// 3. length: 100, width: 50, depth: 25. expected: 72.3379, actual: 72.33796296296298
//  The program results came out as expected.
