package labs.lab4;

public class Yingqi_Jiang_GeoLocationApp_02212022 {

	public static void main(String[] args) {
		// default constructor
		Yingqi_Jiang_GeoLocation_02212022 a = new Yingqi_Jiang_GeoLocation_02212022();
		a.getLat(); // calling accessors
		a.getLat();
		System.out.println(a);
		
		// non-default constructor
		Yingqi_Jiang_GeoLocation_02212022 b = new Yingqi_Jiang_GeoLocation_02212022(90,110);
		b.getLat();
		b.getLat();
		System.out.println(b);
	}

}
