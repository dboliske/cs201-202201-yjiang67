package labs.lab4;

public class Yingqi_Jiang_PhoneNumber_02212022 {
// create variables
	private String countryCode;
	private String areaCode;
	private String number;
	
// default constructor	
	public Yingqi_Jiang_PhoneNumber_02212022() {
		countryCode = "+1" ;
		areaCode = "312" ;
		number = "1234567" ;
	}
// non-default constructor
	public Yingqi_Jiang_PhoneNumber_02212022(String cCode, String aCode, String num) {
		countryCode = cCode;
		areaCode = aCode;
		number = num;
	}
	
// accessors	
	public String getCountryCode() {
		return countryCode;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public String getNumber() {
		return number;
	}
	
// mutators
	public void setCountryCode(String cCode) {
		countryCode = cCode;
	}
	public void setAreaCode(String aCode) {
		areaCode = aCode;
	}
	public void setNumber(String num) {
		number = num;
	}
	
// format method
	public String toString() {
		return "Phone number is " + countryCode + areaCode + number + ".";
	}
	
// checks parameter
	public boolean validAreaCode(String areaCode) {
		int acLength = areaCode.length();
		if(acLength != 3) {
			return false;
		}else {
			return true;
		}
	}
	public boolean validNumber(String number) {
		int numLength = number.length();
		if (numLength != 7) {
			return false;
		} else {
			return true;
		}
	}
	
// compare values 
	public boolean equals(Yingqi_Jiang_PhoneNumber_02212022 pn) {
		if (countryCode != pn.getCountryCode()) {
			return false;
		} else if (areaCode != pn.getAreaCode()) {
			return false;
		}else if (number != pn.getNumber()) {
			return false;
		} else {
			return true;
		}
	}
}
