package labs.lab4;

public class Yingqi_Jiang_Potion_02212022 {
// create variables
	private String name;
	private double strength;
	
// default constructor
	public Yingqi_Jiang_Potion_02212022() {
		name = "Bob";
		strength = 0.99;
	}
// non-default constructor
	public Yingqi_Jiang_Potion_02212022(String n, double s) {
		name = n;
		strength = s;
	}
	
// accessors
	public String getName() {
		return name;
	}
	public double getStrength() {
		return strength;
	}
	
// mutators
	public void setName(String name) {
		this.name = name;
	}
	public void setStrength(double strength) {
		this.strength = strength;
	}
	
// formatting
	public String toString() {
		return "Name: " + name + ". Stength: " + strength + ".";
	}
	
// checks parameter
	public boolean validStrength(double strength) {
		if (strength >= 0 && strength <= 10) {
			return true;
		}else {
			return false;
		}
	}
	
// compare values/instances
	public boolean equals(Yingqi_Jiang_Potion_02212022 x) {
		if (name != x.getName()) {
			return false;
		}else if (strength != x.getStrength()) {
			return false;
		}else {
			return true;
		}
	}
	
}
