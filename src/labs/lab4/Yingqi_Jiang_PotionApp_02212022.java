package labs.lab4;

public class Yingqi_Jiang_PotionApp_02212022 {

	public static void main(String[] args) {
		// Default
		Yingqi_Jiang_Potion_02212022 one = new Yingqi_Jiang_Potion_02212022();
		System.out.println(one.toString()); // calling toStiring method
		
		// Non-default
		Yingqi_Jiang_Potion_02212022 two = new Yingqi_Jiang_Potion_02212022("Marry",9.99);
		System.out.println(two.toString());
		
	}

}
