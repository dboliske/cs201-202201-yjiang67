package labs.lab4;

public class Yingqi_Jiang_GeoLocation_02212022 {
// create variables
	private double lat;
	private double lng;
	
// default constructor	
	public Yingqi_Jiang_GeoLocation_02212022() {
		lat = 0;
		lng = 0;
	}
//	non-default constructor
	public Yingqi_Jiang_GeoLocation_02212022(double lattitude, double longtitude) {
		lat = lattitude;
		lng = longtitude;
	}
	
// accessor methods	
	public double getLat() {
		return lat;
	}
	public double getLng() {
		return lng;
	}
	
// mutator methods	
	public void setLat(double lat) {
			this.lat = lat;
	}
	public void setLng(double lng) {
			this.lng = lng;
	}
	
// format method	
	public String toString() {
		return "Location: ( " + lat + ", " + lng + ")";
	}
	
// boolean methods to check parameters
	public boolean validLat(double lat) {
		if(lat >= -90 && lat <= 90) {
			return true;
		}else {
			return false;
		}
	}
	public boolean validLng(double lng) {
		if(lng >= -180 && lng <= 180) {
			return true;
		}else {
			return false;
		}
	}
	
// boolean method to compare values	
	public boolean equals(Yingqi_Jiang_GeoLocation_02212022 x) {
		if (this.lat != x.getLat()) {
			return false;
		}else if (this.lng != x.getLng()) {
			return false;
		}else {
			return true;
		}
	}
}
