package labs.lab4;

public class Yingqi_Jiang_PhoneNumberApp_02212022 {

	public static void main(String[] args) {
		// Default
		Yingqi_Jiang_PhoneNumber_02212022 pn1 = new Yingqi_Jiang_PhoneNumber_02212022();
		System.out.println(pn1.toString()); // calling toString method
		
		// Non-default 
		Yingqi_Jiang_PhoneNumber_02212022 pn2 = new Yingqi_Jiang_PhoneNumber_02212022("+00","486","5482113");
		System.out.println(pn2.toString());

	}

}
