package labs.lab3;

import java.io.FileWriter;
import java.util.Scanner;

public class Yingqi_Jiang_CS201_Lab3_E2_02072022 {

	public static void main(String[] args) {
		// Exercise 2 
		Scanner input = new Scanner(System.in);
		
		int[] data = new int[1]; //create array
		boolean done = false; // flag variable
		int count = 0; // initialize loop count
		do { //ask user for input repeatedly until they enter done
			System.out.println("Enter a number(enter done when finshed): ");
			String user = input.nextLine();
			switch(user) { 
				case "done":
					done = true; //change flag to end loop once done is entered
					break;
				default:
					if (count == 0) {
						data[count] = Integer.parseInt(user); //change string to int
					}
					if (count == data.length) { 
						int [] larger = new int[data.length + 1]; //update array by 1
						larger[count] = Integer.parseInt(user);
						for (int i=0; i<data.length; i++) {
							larger[i] = data[i]; // making sure the new array includes elements in the old one
						}
						data = larger;
						larger = null;
					}
					
					}
			count++;	// track number of times loop runs
			}while(!done);
		
		System.out.println("Enter a file name: "); //prompt user for file name
		String name = input.nextLine();
		try {
		FileWriter f = new FileWriter("src/labs/lab3/" + name); //write a new file with the name
				for (int k=0; k<data.length; k++) {
					f.write(data[k] + "\n"); //write the array into the file
				}
				f.flush();
				f.close(); 
		} catch(Exception e) {
			System.out.println("Error");
		}
		System.out.println("Ended");
	input.close();		
	}

}
