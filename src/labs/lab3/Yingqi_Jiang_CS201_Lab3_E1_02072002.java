package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Yingqi_Jiang_CS201_Lab3_E1_02072002 {

	public static void main(String[] args) throws IOException {
		// Exercise 1
		File grades = new File("src/labs/lab3/grades.csv");
		Scanner input = new Scanner(grades);
		
		int[] scores = new int[14]; // creates array with # of data in the file
		int loop = 0;
		// runs while loop by checking if there's another line
		while (input.hasNextLine()) {
			String line = input.nextLine();
			String [] data = line.split(","); // splits string into multiple columns with ","
			scores[loop] = Integer.parseInt(data[1]); // stores data in column 2
			loop++;
		}
		
		input.close();
		
		double total = 0;  
		for(int i=0; i<scores.length; i++) {
			total = total + scores[i];
		} // calculate the total of the scores enter by user
		System.out.println("Average score: " + (total/scores.length) + ".");
		// Print out average score
	}

}
