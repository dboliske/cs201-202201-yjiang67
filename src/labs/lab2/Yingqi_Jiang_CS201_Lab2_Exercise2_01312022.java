package labs.lab2;

import java.util.Scanner;

public class Yingqi_Jiang_CS201_Lab2_Exercise2_01312022 {

	public static void main(String[] args) {
		// Exercise 2
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the number of scores: "); // prompt user for # of inputs 
		int num = Integer.parseInt(input.nextLine());
		double[] scores = new double[num]; // create an array 
		for(int i=0; i<scores.length; i++) { 
			System.out.println("score" + (i+1) + ": ");
			scores[i] = Double.parseDouble(input.nextLine());
		} // stores user input in the array
		input.close();
		
		double total = 0;  
		for(int i=0; i<scores.length; i++) {
			total = total + scores[i];
		} // calculate the total of the scores enter by user
		System.out.println("Average score: " + (total/scores.length) + ".");
		// Print out average score
	}

}
