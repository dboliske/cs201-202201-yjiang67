package labs.lab2;

import java.util.Scanner;

public class Yingqi_Jiang_CS201_Lab2_Exercise1_01312022 {

	public static void main(String[] args) {
		// Exercise 1
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a number: ");
		int num = input.nextInt(); // scan user input, store as int
		int loop1 = 0; //initialize outer loop
		int loop2 = 0; // initialize inner loop
		// runs loop for num of times 
		for (loop1 = 0; loop1 != num; loop1++) {
			
			for(loop2 = 0; loop2 != num; loop2++) {
			System.out.print("*"); // prints out * for whatever repeated times user entered
			}
			System.out.print("\n"); // starts a new line every time the loop ends
		}
		
		input.close();
	}

}
