package labs.lab2;

import java.util.Scanner;

public class Yingqi_Jiang_CS201_Lab2_Exercise3_01312022 {

	public static void main(String[] args) {
		// Exercise 3
		Scanner input = new Scanner(System.in);
		
			System.out.println("1.Say Hello"); // print out menu
			System.out.println("2.Addition");
			System.out.println("3.Multiplication");
			System.out.println("4.Exist");
			System.out.println("Option: ");
			String option = input.nextLine(); // scan user option
			
			boolean exist = option.equals("4"); // flag control variable
			while (!exist) {  // repeat the following if user input is not 4
				switch(option){
					case "1":
						System.out.println("Hello");
						break;
					case "2":
						System.out.println("Enter a number: ");
						double x = Double.parseDouble(input.nextLine());
						System.out.println("Enter another number: ");
						double y = Double.parseDouble(input.nextLine());
						double sum = x + y; //addition
						System.out.println("The sum of the two number is " + sum + ".");
						break;
					case "3":
						System.out.println("Enter a number: ");
						double a = Double.parseDouble(input.nextLine());
						System.out.println("Enter another number: ");
						double b = Double.parseDouble(input.nextLine());
						double product = a*b; //multiplication
						System.out.println("The product of the two number is " + product + ".");
						break;
					case "4":
						exist = true;
						break;
					default:
						System.out.println("Invalid");
				}
				// prints out menu again if the user didn't enter 4
				if(!exist) {
					System.out.println("1.Say Hello"); 
					System.out.println("2.Addition");
					System.out.println("3.Multiplication");
					System.out.println("4.Exist");
					System.out.println("Option: ");
					option = input.nextLine(); 
				}
			} System.out.println("Program existed.");
			
		input.close();
	}

}
