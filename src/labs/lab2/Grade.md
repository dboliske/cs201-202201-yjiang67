# Lab 2

## Total

19/20

## Break Down

* Exercise 1    6/6
* Exercise 2    5/6
* Exercise 3    6/6
* Documentation 2/2

## Comments
-1 pt on Exercise 2 because you shouldn't ask the user for the amount of grades they are giving you. You're program should run until the user enters a specific value such as -1 to end their list and calculate the average of the grades they entered.