package labs.lab7;

public class Lab7_InsertionSort_Yingqi_Jiang {
	
	public static String[] sort(String[] array) {
		for (int i=1; i<array.length; i++) {
			int j = i;
			while (j > 0 && array[j].compareTo(array[j-1]) < 0) { //compares array element at j to the previous 
				String x = array[j];
				array[j] = array[j-1];
				array[j-1] = x;          
				j--;
			}
		}
		
		return array;
	}

	public static void main(String[] args) {
		String[] things = {"cat", "fat", "dog", "apple", "bat", "egg"};
		
		things = sort(things);
		
		for (String l : things) {
			System.out.print(l + " ");
		}

	}

}
