package labs.lab7;

import java.util.Scanner;

public class Lab7_BinarySearch_Yingqi_Jiang {
	
	public static int search(String[] array, String value) {
		int start = 0;
		int end = array.length;
		int pos = -1;
		boolean found = false;
		while (!found && start != end) {
			int middle = (start + end) / 2;
			if (array[middle].equalsIgnoreCase(value)) {
				found = true;
				pos = middle;
			} else if (array[middle].compareToIgnoreCase(value) < 0) { 
				start = middle + 1; // value comes after mid index
			} else {
				end = middle; // value comes before index mid
			}
		}
		
		return pos;
	}


	public static void main(String[] args) {
		String[] x = {"c", "html", "java", "python", "ruby", "scala"};
		
		Scanner input = new Scanner(System.in);
		System.out.print("Search for: ");
		String value = input.nextLine();
		int index = search(x, value);
		if (index == -1) {
			System.out.println(value + " not found.");
		} else {
			System.out.println(value + " found at index " + index + ".");
		}

		input.close();
	}
	

}
