package labs.lab7;

public class Lab7_SelectionSort_Yingqi_Jiang {
	// goes through array and sorts by looking for next minimum
	public static double[] sort(double[] array) {
		for (int i=0; i<array.length - 1; i++) {
			int min = i;
			for (int j=i+1; j<array.length; j++) {
				if (array[j] < array[min]) {
					min = j;
				}
			}
			
			if (min != i) {
				double x = array[i];
				array[i] = array[min];
				array[min] = x;        
			}
		}
		
		return array;
	}

	public static void main(String[] args) {
		double[] num = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		num = sort(num);
		
		for (double l : num) {
			System.out.print(l + " ");
		}
	}
}

