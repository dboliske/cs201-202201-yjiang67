package labs.lab7;

public class Lab7_BubbleSort_Yingqi_Jiang {
	
	public static int[] sort(int[] array) {
		boolean done = false;
		
		do {
			done = true;
			for (int i=0; i<array.length-1; i++) {
				if (array[i+1]< array[i]) { // compare + sort every elements in the array
					int x = array[i+1];
					array[i+1] = array[i];
					array[i] = x;
					done = false;
				}
			}
		} while (!done);
		
		return array;
	}

	public static void main(String[] args) {
		int[] num = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		num = sort(num);
		for (int l:num) {    //prints out each element of array in order
			System.out.print(l + " ");
		}

	}

}
