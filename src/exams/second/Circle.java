package exams.second;

public class Circle extends Polygon{

	private double radius;
	
	public Circle() {
		radius = 1;
	}
	
	public void setRadius(double width) {
		radius = width;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public String toString() {
		return "The circle has a radius of " + radius + ".";
	}
	
	@Override
	public double area() {
		return Math.PI * radius * radius;
	}
	
	@Override
	public double perimeter() {
		return 2 * Math.PI * radius;
	}
	
	
}
