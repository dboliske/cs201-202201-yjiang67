package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class Question_Three {

	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		ArrayList<Double> numbers = new ArrayList<Double>();
		boolean done = false;
		do {
			System.out.println("Enter a number(enter Done when finished): ");
			String in = input.nextLine();
			switch (in) {
			case "Done":
				done = true;
				System.out.println("Program ended.");
				break;
			default : 
				numbers.add(Double.parseDouble(in));
			}
			
		} while (!done);
		
		double min = numbers.get(0);
		double max = numbers.get(0);
		for (int i=0; i<numbers.size(); i++) {
			if(numbers.get(i) < min) {
				min = numbers.get(i);
			}
			if(numbers.get(i) > max) {
				max = numbers.get(i);
			}
		}
		System.out.println("The minimum value is " + min);
		System.out.println("The maximum value is " + max);
		input.close();
	}

}
