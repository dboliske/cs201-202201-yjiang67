package exams.second;

public class Classroom extends ComputerLab{
	protected String building;
	protected String roomNumber;
	private int seats;
	
	public Classroom() {
		building = "A1";
		roomNumber = "A123";
		seats = 25;
	}
	
	public void setBuilding(String building) {
		this.building = building;
	}
	
	public void setRoomNumber(String number) {
		roomNumber = number;
	}
	
	public void setSeats(int seats) {
		if (seats >= 0) {
			this.seats = seats;
		} else {
			System.out.println("Ivalid number of seats.");
		}
		
	}
	
	public String getBuilding() {
		return building;
	}
	
	public String getRoomNumber() {
		return roomNumber;
	}
	
	public int getSeats() {
		return seats;
	}
	
	@Override
	public String toString() {
		return super.toString() + " Building " + building + ". Room " + roomNumber + ". " + seats + " seats.";
	}
}
