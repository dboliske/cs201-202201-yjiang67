package exams.second;

public class Rectangle extends Polygon {

	private double width;
	private double height;
	
	public Rectangle() {
		width = 1;
		height = 1;
	}
	
	public void setWidth(double width) {
		this.width = width;
	}
	
	public void setHeight(double height) {
		this.height = height;
	}
	
	public double getWidth() {
		return width;
	}
	
	public double getHeight() {
		return height;
	}
	
	public String toString() {
		return "The rectangle has a width of " + width + " and a height of " + height + ".";
		}
	
	@Override
	public double area() {
		return (height*width);
	}
	
	@Override 
	public double perimeter() {
		return 2 * (height + width);
	}
	
	
	
}
