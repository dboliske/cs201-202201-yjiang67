package exams.second;

import java.util.Scanner;

public class Question_Five {

	public static int jumpSearch(double[] array, double value) {
		int n = (int)Math.sqrt(array.length);
		int pos = n-1;
		
		if((pos) < array.length && value > array[pos]) {
			pos += n;
			
		} else {
			for(int i=0; i<=(pos) && i<array.length; i++) {
				if(value == array[i]) {
					return i;
				}
			}
		}
		return -1;
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[] num = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		System.out.println("Search for a number: ");
		double a = Double.parseDouble(input.nextLine());
		int pos = jumpSearch(num, a);
		if (pos == -1) {
			System.out.println("Number not found.");
		} else {
			System.out.println(a + " found at position: " + pos);
		}
 input.close();
		}
	}

