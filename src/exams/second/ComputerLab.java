package exams.second;

public class ComputerLab {
	
	private boolean computers;
	
	public ComputerLab() {
		computers = true;
	}
	
	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	public boolean hasComputers() {
		if (computers == true) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override 
	public String toString() {
		if (computers == true) {
			return "The computer lab has computers.";
		} else {
			return "The computer lab has no computers.";
		}
	}
}
