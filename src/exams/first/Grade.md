# Midterm Exam

## Total

90/100

## Break Down

1. Data Types:                  20/20
    - Compiles:                 5/5
    - Input:                    5/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   20/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  5/5
3. Repetition:                  17/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               2/5
4. Arrays:                      16/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  1/5
5. Objects:                     17/20
    - Variables:                5/5
    - Constructors:             3/5
    - Accessors and Mutators:   5/5
    - toString and equals:      4/5

## Comments

1. Good
2. Good
3. On the right track, but formatting is off.
4. On the right track again, but doesn't correctly compare words to find repeats.
5. Non-default constructor does not validate `age` and `equals` method doesn't take an Object.
