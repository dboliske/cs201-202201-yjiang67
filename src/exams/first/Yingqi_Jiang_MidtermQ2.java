package exams.first;

import java.util.Scanner;

public class Yingqi_Jiang_MidtermQ2 {

	public static void main(String[] args) {
		// Question 2
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a integer: ");
		int num = Integer.parseInt(input.nextLine());
		if ((num % 2 == 0) && (num % 3 == 0)) {
			System.out.println("foobar");
		} else if (num % 2 == 0) {
			System.out.println("foo");
		} else if (num % 3 == 0) {
			System.out.println("bar");
		}
		input.close();
		}

}
