package exams.first;

public class Pet {

	private String name;
	private int age;
	
	public Pet() {
		name = "Bob";
		age = 10;
		}
	public Pet(String n, int a) {
		name = n;
		age = a;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public void setAge(int age) {
		if (age > 0) {
			this.age = age;
		} else {
			System.out.println("invalid age");
		}
	}
	
	public String getName() {
		return name;
	}
	public int getAge() {
		return age;
	}
	
	public boolean equals(Pet obj) {
		if(this.name != obj.getName()) {
			return false;
		} else if (this.age != obj.getAge()) {
			return false;
		} else {
			return true;
		}
	}
	
	public String toString() {
		return "Pet name: " + name + " Pet age: " + age;
	}
}
