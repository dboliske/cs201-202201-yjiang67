package project;

public class ProduceItem extends Stocks {
	// variables
	private int day;
	private int mon;
	private int yr;
	
	// default constructor
	public ProduceItem() {
		super();
		day = 29;
		mon = 4;
		yr = 2022;
	}
	// non -default constructor
	public ProduceItem(String name, double price, int day, int mon, int yr) {
		super(name, price);
		if (day > 0 && day <= 31) {
			this.day = day;
		}
		if(mon > 0 && mon <= 12) {
			this.mon= mon;
		}
		if (yr > 0) {
			this.yr = yr;
		}
	}
	
	// acessors
	public int getDay() {
		return day;
	}
	public int getMon() {
		return mon;
	}
	public int getYr() {
		return yr;
	}
	
	// mutators
	public void setDay(int day) {
		if (day > 0 && day <= 31) {
			this.day = day;
		}
		System.out.println("Invalid day");
	}
	public void setMon(int mon) {
		if(mon > 0 && mon <= 12) {
			this.mon= mon;
		}
		System.out.println("Invalid month");
	}
	public void setYr(int yr) {
		if (yr > 0) {
			this.yr = yr;
		}
		System.out.println("Invalid year");
	}
		
	
	@Override
	public String toString() {
		return super.toString() + " The expiration date of the " + super.getName() + " is " 
	     + yr + "-" + (mon<10?("0" + mon): mon) + "-" + (day<10?("0" + day): day) + ".";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof ProduceItem)) {
			return false;
		}
		
		ProduceItem p = (ProduceItem)obj;
		if (this.yr != p.getYr()) {
			return false;
		} else if (this.mon != p.getMon()) {
			return false;
		} else if (this.day != p.getDay()) {
			return false;
		}
		
		return true;
	}

	@Override
	public String toCSV() {
		return super.csvData() + "," + mon + "/" + day + "/" + yr;
	}
	
	
}
