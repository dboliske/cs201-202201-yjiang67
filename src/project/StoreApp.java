package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class StoreApp {
	// create list for cart(contains index of each item to remove from the stock)
	private static ArrayList<Integer> cart = new ArrayList<Integer>();
	
	// read file
	public static ArrayList<Stocks> readFile(String filename) {
		ArrayList<Stocks> stocks = new ArrayList<Stocks>();
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String[] values = line.split(",");
					Stocks a = null;
					switch (values[0].toLowerCase()) {
					case "banana":
					case "carrot":
					case "apple":
					case "avacado":
					case "bread":
					case "bag of chips":
					case "bottle of water":
						String[] dateValues = values[2].split("/");
						a = new ProduceItem(values[0],
											Double.parseDouble(values[1]), 
											Integer.parseInt(dateValues[1]),
											Integer.parseInt(dateValues[0]),
											Integer.parseInt(dateValues[2]));
						break;
					case "pencil":
					case "pen":
					case "pad of paper":
						a = new ShelvedItem(values[0], Double.parseDouble(values[1]));
						break;
					case "wine":
					case "beer":
					case "cold meds":
						a = new AgeResItem(values[0],
											Double.parseDouble(values[1]),
											Integer.parseInt(values[2]));
						break;
					}
					stocks.add(a);
				} catch (Exception E) {
					
				}
			}
			input.close();
		}catch (FileNotFoundException fnf) {
			System.out.println("File not found");
		} catch(Exception e) {
			System.out.println("Error reading in file");
		}
		
		return stocks;
	}
	
	// resize method
	public static Stocks[] resize(Stocks[] data, int size) {
		Stocks[] temp = new Stocks[size];
		int limit = data.length > size ? size : data.length;
		for (int i=0; i<limit; i++) {
			temp[i] = data[i];
		}
		
		return temp;
	}

	// Menu
	public static ArrayList<Stocks> menu(Scanner input, ArrayList<Stocks> data) {
		boolean exist = false;
		
		do {
			System.out.println("1. Restock Item");
			System.out.println("2. Add Item to cart");
			System.out.println("3. Checking out items in cart");
			System.out.println("4. Search for an item");
			System.out.println("5. Update price of an item");
			System.out.println("6. Update name of an item");
			System.out.println("7. Exist");
			System.out.println("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
			 case "1":
				 data = restockItem(data, input);
				 break;
			 case "2":
				 data = addToCart(data,input);
				 System.out.println("Item added to cart.");
				 break;
			 case "3":
				 data = CheckOutCart(data);
				 break;
			 case "4":
				 searchItem(data,input);
				 break;
			 case "5":
				 data = reprice(data, input);
				 break;
			 case "6":
			 	 data = rename(data, input);
			 	 break;
			 case "7":
				 exist = true;
				 break;
			 default:
				 System.out.println("Invalid choice.");
			}
		}while(!exist);
			
		return data;
	}
	
	// Restock Items method
	public static ArrayList<Stocks> restockItem(ArrayList<Stocks> data, Scanner input) {
		Stocks a;
		System.out.println("Enter item name: ");
		String name = input.nextLine();
		System.out.println("Enter price of item: ");
		double price = 0;
		try {
			price = Double.parseDouble(input.nextLine());
		} catch (Exception e) {
			System.out.println("Not a valid price, returning to menu.");
			return data;
		}
		System.out.println("Type of item (Produce, Shelved, Age Restricted): ");
		String type = input.nextLine();
		switch(type.toLowerCase()) {
			case "produce":
				System.out.println("Year of expiry date: ");
				int yr = Integer.parseInt(input.nextLine());
				if (yr <= 0) {
					System.out.println("Not a valid year, return to menu.");
					return data;
				}
				System.out.println("Month of expiry date: ");
				int mon = Integer.parseInt(input.nextLine());
				if (mon <= 0 || mon > 12) {
					System.out.println("Not a valid month, return to menu.");
					return data;
				}
				System.out.println("Day of expiry date: ");
				int d = Integer.parseInt(input.nextLine());
				if (d <= 0 || d > 31) {
					System.out.println("Not a valid day, return to menu.");
					return data;
				}
				a = new ProduceItem(name, price, d, mon, yr);
				break;
			case "shelved":
				a = new ShelvedItem(name, price);
				break;
			case "age restricted":
				System.out.println("Enter the age required to purchase this item: ");
				int age = Integer.parseInt(input.nextLine());
				if (age <= 0) {
					System.out.println("Not a valid age");
					return data;
				}
				a = new AgeResItem(name, price, age);
				break;
			default:
				System.out.println("Not a valid item type, returning to menu.");
				return data;
		}
		data.add(a);
		return data;
	}
	
	// Add items to cart
	public static ArrayList<Stocks> addToCart(ArrayList<Stocks> data, Scanner input){
		System.out.println("Enter name of item to add to cart: ");
		String name = input.nextLine();
		for (Stocks a: data) {
			boolean addCart = a.getName().equalsIgnoreCase(name);
			if (addCart) {
				int index = data.indexOf(a.getName());
				cart.add(index);
				break;
			} else {
				System.out.println("Item not found.");
			}
		}
		return data;
	}
	
	// Checking out items in cart
	public static ArrayList<Stocks> CheckOutCart(ArrayList<Stocks> data){
		for(int i = 0; i<cart.size(); i++) {
			data.remove(cart.get(i));
		}
		return data;
	}
	
	// Search for an item 
	public static void searchItem(ArrayList<Stocks> data, Scanner input){
		System.out.println("Search for(name of item): ");
		String item = input.nextLine();
		for(Stocks a: data) {
			boolean found = a.getName().equalsIgnoreCase(item);
			if(found) {
				System.out.println(item + " found in stock. " + "The price is $" + a.getPrice() + ".");
			} else {
				System.out.println("Item not found.");
			}
			break;
		}
	}
		
	// Update Price of an Item
	public static ArrayList<Stocks> reprice(ArrayList<Stocks> data, Scanner input){
		System.out.println("Enter name of item for updating the price: ");
		String item = input.nextLine();
		System.out.println("Enter the new price of the item: ");
		double price = Double.parseDouble(input.nextLine());
		for (Stocks a: data) {
			if (a.getName().equalsIgnoreCase(item)) {
				a.setPrice(price);
			} else {
				System.out.println("Item not found.");
			}
		}
		return data;
	}

	// Update Name of Item
	public static ArrayList<Stocks> rename(ArrayList<Stocks> data, Scanner input){
		System.out.println("Enter name of item for updating the name: ");
		String item = input.nextLine();
		System.out.println("Enter the new name of the item: ");
		String name = input.nextLine();
		for (Stocks a: data) {
			if (a.getName().equalsIgnoreCase(item)) {
				a.setName(name);
			} else {
				System.out.println("Item not found.");
			}
		}
		return data;
	}
	
	// Save File
	public static void saveFile(String filename, ArrayList<Stocks> data) {
		try {
			FileWriter writer = new FileWriter(filename);
			
			for (Stocks a : data) {
				writer.write(a.toCSV() + "\n");
				writer.flush();
			}
			
			writer.close();
		} catch (Exception e) {
			System.out.println("Error saving to file.");
		}
	}
	

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Load file(name): ");
		String filename = input.nextLine();
		// Load file (if exists)
		ArrayList<Stocks> data = readFile(filename);
		// Menu
		data = menu(input, data);
		// Save file
		saveFile(filename, data);
		input.close();
		System.out.println("Program Ended.");
		}
	}
