package project;

public class Stocks {
	// variables
	private String name;
	private double price;
	
	// constructors
	public Stocks() {
		name = "banana";
		price = 0.62;
	}
	public Stocks(String name, double price) {
		this.name = name;
		if (price >= 0 ) {
			this.price = price;
		}
		System.out.println("Invalid price");
	}
	
	// accessors
	public String getName() {
		return name;
	}
	public double getPrice() {
		return price;
	}
	
	// mutators
	public void setName(String name) {
		this.name = name;
	}
	public void setPrice(double price) {
		if (price >= 0 ) {
			this.price = price;
		}
	}
	
@Override
	public String toString() {
		return name + " costs $" + price + " dollars.";
	}
	
	protected String csvData() {
		return name + "," + price;
	}
	
	public String toCSV() {
		return csvData();
	}

@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Stocks)) {
			return false;
		}
		
		Stocks a = (Stocks)obj;
		if (!this.name.equalsIgnoreCase(a.getName())) {
			return false;
		} else if (this.price != a.getPrice()) {
			return false;
		}
		
		return true;
	}
	
	
}
