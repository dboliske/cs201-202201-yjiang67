package project;

public class AgeResItem extends Stocks {
	// variable
	private int age;
	
	// constructors
	public AgeResItem() {
		super();
		age = 21;
	}
	public AgeResItem(String name, double price, int age) {
		super(name, price);
		this.age = age;
	}
	
	// accessor
	public int getAge() {
		return age;
	}
	
	// mutator
	public void setAge(int age) {
		if (age > 0) {
			this.age = age;
		}
		System.out.println("Invalid age");
	}
	
	@Override
	public String toString() {
		return super.toString() + " This item requires the customer to be at least " 
																+ age + " years old.";
	}
	
	@Override 
	public String toCSV() {
		return super.csvData() + "," + age;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof AgeResItem)) {
			return false;
		}
		
		AgeResItem a = (AgeResItem)obj;
		if (this.age != a.getAge()) {
			return false;
		}
		return true;
	}
	
}
