package project;

public class ShelvedItem extends Stocks {
	// constructors
	public ShelvedItem() {
		super();
	}
	public ShelvedItem(String name, double price) {
		super(name, price);
	}
	
	@Override
	public boolean equals(Object obj) {
		if (!super.equals(obj)) {
			return false;
		} else if (!(obj instanceof ShelvedItem)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + super.getName() + " is an shelved item.";
	}
	
	@Override
	public String toCSV() {
		return super.csvData();
	}
}
